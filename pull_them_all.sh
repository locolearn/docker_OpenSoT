#!/bin/sh

echo "This script aim to git pull all the needed repository to build the docker images"
echo "Do not put git pull in the Dockerfile"

echo "Pulling for dockerfile  : base"

echo "Pulling for dockerfile  : gui"

echo "Pulling for dockerfile  : common"

echo "Pulling for dockerfile  : ipopt"

echo "Pulling for dockerfile  : yarp"

echo "Pulling for dockerfile  : icub-main"
# icub-main
if [ ! -d "./icub-main/icub-main" ] || [ -z "$(ls -A ./icub-main/icub-main)" ]; then
  git clone https://github.com/robotology/icub-main ./icub-main/icub-main
  cd ./icub-main/icub-main
  # Pulling commit from Apr 19, 2018
  git checkout dd83e600e6ed357fee6757aa4f5897687525ab4b
  cd ../..
fi
# tutorials for icub-main
if [ ! -d "./icub-main/icub-tutorials" ] || [ -z "$(ls -A ./icub-main/icub-tutorials)" ]; then
  git clone https://github.com/robotology/icub-tutorials ./icub-main/icub-tutorials
  cd ./icub-main/icub-tutorials
  git checkout 4b2d883f2805ef84e3de18d361d3fa566134f455
  cd ../..
fi

cd ./icub-main/icub-main
git pull origin master
cd ../..

echo "Pulling for dockerfile  : ros"

echo "Pulling for dockerfile  : OpenSoT"
# matio
if [ ! -d "./OpenSoT/matio" ] || [ -z "$(ls -A ./OpenSoT/matio)" ]; then
  git clone https://github.com/tbeu/matio.git ./OpenSoT/matio
fi

# OpenSoT-superbuild
if [ ! -d "./OpenSoT/OpenSoT-superbuild" ] || [ -z "$(ls -A ./OpenSoT/OpenSoT-superbuild)" ]; then
  git clone https://github.com/EnricoMingo/OpenSoT-superbuild.git ./OpenSoT/OpenSoT-superbuild
  cd ./OpenSoT/OpenSoT-superbuild
  # Pulling commit from Mar 30, 2018
  git checkout 0fae2fdff1b7d03b3b8add3f12fea0d265954fc4
  cd ../..
fi

cd ./OpenSoT/OpenSoT-superbuild
git pull origin master
cd ../..
# configuration files for icub in OpenSoT
if [ ! -d "./OpenSoT/opensot_configs" ] || [ -z "$(ls -A ./OpenSoT/opensot_configs)" ]; then
  git clone https://gitlab.inria.fr/locolearn/opensot_configs.git ./OpenSoT/opensot_configs
fi

echo "Pulling for dockerfile  : simulation"
# Gazebo stuff needed
if [ ! -d "./simulation/gazebo_ros_pkgs" ] || [ -z "$(ls -A ./simulation/gazebo_ros_pkgs)" ]; then
  git clone https://github.com/ros-simulation/gazebo_ros_pkgs.git ./simulation/gazebo_ros_pkgs
fi

cd ./simulation/gazebo_ros_pkgs
git pull origin master
cd ../..

if [ ! -d "./simulation/gazebo-yarp-plugins" ] || [ -z "$(ls -A ./simulation/gazebo-yarp-plugins)" ]; then
  git clone https://github.com/robotology/gazebo-yarp-plugins.git ./simulation/gazebo-yarp-plugins
  cd ./simulation/gazebo-yarp-plugins
  # Pulling commit from Feb 12, 2018
  git checkout 5f595c10321b7afae4cc4c89f23fc6c8c6a23493
  # Pulling commit from Jan 29, 2018
  #git checkout 7e5e25b157c5e771fca242e7354b944f1eb3d755
  cd ../..
fi

cd ./simulation/gazebo-yarp-plugins
git pull origin master
cd ../..

# Description package for iCub
if [ ! -d "./simulation/icub_description" ] || [ -z "$(ls -A ./simulation/icub_description)" ]; then
  git clone https://gitlab.inria.fr/larsen-robots/icub_description.git ./simulation/icub_description
fi

# Description package for the 2D box
if [ ! -d "./simulation/box_description" ] || [ -z "$(ls -A ./simulation/box_description)" ]; then
  git clone https://gitlab.inria.fr/locolearn/box_description.git ./simulation/box_description
fi

# Tiago package for HIS
if [ ! -d "./simulation/tiago_his" ] || [ -z "$(ls -A ./simulation/tiago_his)" ]; then
  git clone https://gitlab.inria.fr/refonte-appartement/tiago_his.git ./simulation/tiago_his
fi

# tiago_tests package for HIS
if [ ! -d "./simulation/tiago_tests" ] || [ -z "$(ls -A ./simulation/tiago_tests)" ]; then
  git clone https://gitlab.inria.fr/refonte-appartement/tiago_tests.git ./simulation/tiago_tests
fi

# robot_framework_msgs package for HIS
if [ ! -d "./simulation/robot_framework_msgs" ] || [ -z "$(ls -A ./simulation/robot_framework_msgs)" ]; then
  git clone https://gitlab.inria.fr/locolearn/robot_framework_msgs.git ./simulation/robot_framework_msgs
fi

# robot_framework_controller package for HIS
if [ ! -d "./simulation/robot_framework_controller" ] || [ -z "$(ls -A ./simulation/robot_framework_controller)" ]; then
  git clone https://gitlab.inria.fr/locolearn/robot_framework_controller.git ./simulation/robot_framework_controller
fi

# Package for robot with controller and Bridge for Gazebo
if [ ! -d "./simulation/robot_framework" ] || [ -z "$(ls -A ./simulation/robot_framework)" ]; then
  git clone https://gitlab.inria.fr/locolearn/robot_framework.git ./simulation/robot_framework
fi

cd ./simulation/robot_framework
git pull origin master
cd ../..

echo "Pulling for dockerfile  : learning"
# Mismatch learning repository
if [ ! -d "./learning/mismatch-learning" ] || [ -z "$(ls -A ./learning/mismatch-learning)" ]; then
  git clone https://gitlab.inria.fr/locolearn/mismatch-learning.git ./learning/mismatch-learning
fi

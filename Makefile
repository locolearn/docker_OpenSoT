U_ID := $(shell id -u)
G_ID := $(shell id -g)
NVIDIA_DPKG := $(shell dpkg -l | grep nvidia|grep ii|head -n 1|cut -d " "  -f 3|cut -d '-' -f 2)
NVIDIA_VERSION := $(shell apt-show-versions nvidia-$NVIDIA_DPKG | grep -o '$NVIDIA_DPKG.[0-9][0-9]')

DOCKER_ROOT:=$(shell pwd)

all: help

help:
	@echo ""
	@echo "-- Help Menu"
	@echo ""
	@echo "   ° build                   --- build QP framework"
	@echo "   ° build_his               --- build QP framework for HIS"
	@echo "   ° build_nvidia_no_cache   --- build Nvidia image only without cache"
	@echo "   ° build_nvidia            --- build QP framework with nvidia-384 drivers"
	@echo "   ° build_his_nvidia        --- build QP framework for HIS with nvidia-384 drivers"
	@echo "   ° squash                  --- docker-squash inria/robot-framework:latest"
	@echo "   ° squash_his              --- docker-squash inria/his-robot-framework:latest"
	@echo ""


build:
	./pull_them_all.sh
  ifeq ($(shell test -e ${DOCKER_ROOT}/simulation/Dockerfile && echo -n yes),yes)
		rm ${DOCKER_ROOT}/simulation/Dockerfile
  endif
	ln -s ${DOCKER_ROOT}/simulation/DockerfileIcub ${DOCKER_ROOT}/simulation/Dockerfile
  ifeq ($(shell test -e ${DOCKER_ROOT}/ros/Dockerfile && echo -n yes),yes)
		rm ${DOCKER_ROOT}/ros/Dockerfile
  endif
	ln -s ${DOCKER_ROOT}/ros/DockerfileIcub ${DOCKER_ROOT}/ros/Dockerfile;
  ifeq ($(shell test -e ${DOCKER_ROOT}/OpenSoT/Dockerfile && echo -n yes),yes)
		rm ${DOCKER_ROOT}/OpenSoT/Dockerfile
  endif
	ln -s ${DOCKER_ROOT}/OpenSoT/DockerfileIcub ${DOCKER_ROOT}/OpenSoT/Dockerfile;
  ifeq ($(shell test -e ${DOCKER_ROOT}/nvidia/Dockerfile && echo -n yes),yes)
		rm ${DOCKER_ROOT}/nvidia/Dockerfile
  endif
	ln -s ${DOCKER_ROOT}/nvidia/DockerfileIcub ${DOCKER_ROOT}/nvidia/Dockerfile;
	@docker build --build-arg USER_ID=$(U_ID) --build-arg USER_GID=$(G_ID) --tag=inria/base base/.
	@docker build --tag=inria/gui gui/.
	@docker build --tag=inria/common common/.
	@docker build --tag=inria/ipopt ipopt/.
	@docker build --tag=inria/icub-yarp yarp/.
	@docker build --tag=inria/icub-main icub-main/.
	@docker build --build-arg ROS_DISTRO=kinetic --tag=inria/ros ros/.
	@docker build --tag=inria/opensot OpenSoT/.
	@docker build --tag=inria/simulation simulation/.
	@docker-squash -t inria/simulation:squashed inria/simulation:latest
	@docker build --tag=inria/learning learning/.
	@docker build --tag=inria/robot-framework .

# don't replace spaces by tabulation before ifeq
build_his:
	./pull_them_all.sh
  ifeq ($(shell test -e ${DOCKER_ROOT}/simulation/Dockerfile && echo -n yes),yes)
		rm ${DOCKER_ROOT}/simulation/Dockerfile
  endif
	ln -s ${DOCKER_ROOT}/simulation/DockerfileHIS ${DOCKER_ROOT}/simulation/Dockerfile
  ifeq ($(shell test -e ${DOCKER_ROOT}/ros/Dockerfile && echo -n yes),yes)
		rm ${DOCKER_ROOT}/ros/Dockerfile
  endif
	ln -s ${DOCKER_ROOT}/ros/DockerfileHIS ${DOCKER_ROOT}/ros/Dockerfile;
  ifeq ($(shell test -e ${DOCKER_ROOT}/OpenSoT/Dockerfile && echo -n yes),yes)
		rm ${DOCKER_ROOT}/OpenSoT/Dockerfile
  endif
	ln -s ${DOCKER_ROOT}/OpenSoT/DockerfileHIS ${DOCKER_ROOT}/OpenSoT/Dockerfile;
  ifeq ($(shell test -e ${DOCKER_ROOT}/nvidia/Dockerfile && echo -n yes),yes)
		rm ${DOCKER_ROOT}/nvidia/Dockerfile
  endif
	ln -s ${DOCKER_ROOT}/nvidia/DockerfileHIS ${DOCKER_ROOT}/nvidia/Dockerfile;
	@docker build --build-arg USER_ID=$(U_ID) --build-arg USER_GID=$(G_ID) --tag=inria/base base/.
	@docker build --tag=inria/gui gui/.
	@docker build --tag=inria/common common/.
	@docker build --tag=inria/ipopt ipopt/.
	@docker build --build-arg ROS_DISTRO=kinetic --tag=inria/his-ros ros/.
	@docker build --tag=inria/his-opensot OpenSoT/.
	@docker build --tag=inria/his-simulation simulation/.
	@docker tag inria/his-simulation inria/his-robot-framework

build_nvidia: build squash
	@docker build --build-arg NVIDIA_DPKG=$(NVIDIA_DPKG) --build-arg NVIDIA_VERSION=$(NVIDIA_VERSION) --tag=inria/robot-framework:nvidia nvidia/.

build_his_nvidia: build_his squash_his
	@docker build --build-arg NVIDIA_DPKG=$(NVIDIA_DPKG) --build-arg NVIDIA_VERSION=$(NVIDIA_VERSION) --tag=inria/his-robot-framework:nvidia nvidia/.

build_nvidia_no_cache:
	@docker build --no-cache --build-arg NVIDIA_DPKG=$(NVIDIA_DPKG) --build-arg NVIDIA_VERSION=$(NVIDIA_VERSION) --tag=inria/robot-framework:nvidia nvidia/.

squash:
	@docker-squash -t inria/robot-framework:squashed inria/robot-framework:latest

squash_his:
	@docker-squash -t inria/his-robot-framework:squashed inria/his-robot-framework:latest
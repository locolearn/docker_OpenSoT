# Install Docker

Follow the instructions [here](https://docs.docker.com/engine/installation/linux/docker-ce/ubuntu/) to install Docker.  

then [here](https://docs.docker.com/engine/installation/linux/linux-postinstall/) to configure your user group docker.  

Be sure that your ssh key are set so you can pull on inria repositories and github.  

# Get an image built (Easy way)

You can use [Container Registry](https://gitlab.inria.fr/locolearn/docker_OpenSoT/container_registry) of our GitLab to get an image with the robot_framework for OpenSot.  
First login with :

```
docker login registry.gitlab.inria.fr
```

Then pull image :  

```
docker pull registry.gitlab.inria.fr/locolearn/docker_opensot
```

And if you want an image with nvidia-drivers 384 :  
```
docker pull registry.gitlab.inria.fr/locolearn/docker_opensot:nvidia-384
```

PLEASE READ UNTIL THE END.  

# Run your Docker :  

If you are running your Docker image for the first time you can use the same script as __run_framework.sh__, just copy this in a file called __run_framework.sh__ :
```
#!/bin/sh

USER_UID=$(id -u)
USER_GID=$(id -g)
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth

DOCKER_ROOT="$(pwd)"

if [ -z $1 ]; then
  echo "Running inria/robot_framework:latest image"
  TAG="latest"
else
  echo "Running inria/robot_framework:$1 image"
  TAG="$1"
fi

docker run \
  -it \
  -p 8888:8888 \
  -w /home/icubuser \
  --privileged=true \
  --net=host \
  --volume=/tmp/.X11-unix:/tmp/.X11-unix:rw \
  --volume=/tmp/.docker.xauth:/tmp/.docker.xauth:rw \
  --volume=/dev/bus/usb:/dev/bus/usb:rw \
  --volume=/dev:/dev:rw \
  --volume=/home/$USER/.config/dconf:/home/icubuser/.config/dconf:rw \
  --volume=/home/$USER/.config/gedit:/home/icubuser/.config/gedit:rw \
  --volume=/home/$USER/.config/terminator:/home/icubuser/.config/terminator:rw \
  --env="XAUTHORITY=${XAUTH}" \
  --env="USER_UID=${USER_UID}" \
  --env="USER_GID=${USER_GID}" \
  --env="DISPLAY=${DISPLAY}" \
  --name=openSoTframework \
  registry.gitlab.inria.fr/locolearn/docker_opensot:${TAG} /bin/bash -l

export containerId='docker ps -l -q'
```

Then do a chmod command on your file :  
```
sudo chmod +x run_framework.sh
```

## Options to run your container :  
This option mean that you will mount a volume inside docker in read and write, so everything you will type in */absolute/path/on/your/local/machine* will be the same inside your docker at _/absolute/path/in/the/container_. Aswell if you move, create or delete files. This is usefull for folders on which you are working in.  

```
--volume=/absolute/path/on/your/local/machine:/absolute/path/in/the/container:rw \

```  

Be carefull, if you don't do this and modify your files on your local machine, the changes will not be applied inside your docker.  

This is the name of your container :  
```
--name=openSoTframework \
```  
And the next line is the name of the image you want to run :  
```
inria/opensot:framework \
```  
registry.gitlab.inria.fr/locolearn/docker_opensot : image  
${TAG} : tag of your image  
openSoTframework : name of your container

You can have several container from the same image, just change the _--name_ parameter. The commands *docker ps* list all containers and *docker images* list all images on your local machine.  

If you already ran a container with the script, you can stop your container with *docker stop nameOfYourContainer* command. Then to come back inside, you need to start it with *docker start nameOfYourContainer* and *docker attach nameOfYourContainer* to be inside. Press enter twice after docker attach command.  

To run your container simply do :  
```
./run_framework.sh
```

To run an nvidia container simply do :  
```
./run_framework.sh nvidia-384
```

# Remove containers or images :

To remove an image do *docker rmi idOfYourImage*, and to remove a container do *docker rm nameOfYourContainer*.
Be carefull, removing a container will lose all change inside your container, make sure that you have all files needed on your local machine before removing !  

# Build manually your Docker :  

Go inside docker folder, you should have a __Makefile__, then run in a terminal :  

```make build```  

If you are starting it from scratch it will take time (20 min atleast).

If you have Nvidia drivers on your local machine you can run :

```make build_nvidia```

This command should detect your drivers and install the sames in the images.  
Make sure that your Nvidia drivers have the same version on your local machine and inside the containers.  

If you have any issue ask to me at brice.clement@inria.fr  

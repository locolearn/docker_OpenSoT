#!/bin/sh

USER_UID=$(id -u)
USER_GID=$(id -g)
XSOCK=/tmp/.X11-unix
XAUTH=/tmp/.docker.xauth

DOCKER_ROOT="$(pwd)"

if [ -z $1 ]; then
  echo "Running inria/robot_framework:latest image"
  TAG="latest"
else
  echo "Running inria/robot_framework:$1 image"
  TAG="$1"
fi

docker run \
  -it \
  -w /home/icubuser \
  --privileged=true \
  --net=host \
  --volume=/tmp/.X11-unix:/tmp/.X11-unix:rw \
  --volume=/tmp/.docker.xauth:/tmp/.docker.xauth:rw \
  --volume=/home/$USER/.config/dconf:/home/icubuser/.config/dconf:rw \
  --volume=/home/$USER/.config/gedit:/home/icubuser/.config/gedit:rw \
  --volume=/home/$USER/.config/gedit:/home/icubuser/.config/blender:rw \
  --volume=/home/$USER/.config/terminator:/home/icubuser/.config/terminator:rw \
  --volume=/home/$USER/Programmes/pycharm:/home/icubuser/pycharm:rw \
  --volume=/home/$USER/Programmes/blender:/home/icubuser/blender:rw \
  --volume=$DOCKER_ROOT/simulation/robot_framework:/home/icubuser/robot_framework:rw \
  --volume=$DOCKER_ROOT/simulation/tiago_his:/home/icubuser/catkin_ws/src/tiago_his:rw \
  --volume=$DOCKER_ROOT/simulation/robot_framework_controller:/home/icubuser/catkin_ws/src/robot_framework_controller:rw \
  --volume=$DOCKER_ROOT/simulation/robot_framework_msgs:/home/icubuser/catkin_ws/src/robot_framework_msgs:rw \
  --volume=$DOCKER_ROOT/simulation/tiago_tests:/home/icubuser/tiago_tests:rw \
  --env="XAUTHORITY=${XAUTH}" \
  --env="USER_UID=${USER_UID}" \
  --env="USER_GID=${USER_GID}" \
  --env="DISPLAY=${DISPLAY}" \
  --name=Robotframework \
  inria/his-robot-framework:${TAG} /bin/bash -l

export containerId='docker ps -l -q'

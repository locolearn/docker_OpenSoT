# Ubuntu base image
FROM ubuntu:16.04

# Dockerfile maintainer
MAINTAINER Brice Clement, <brice.clement@inria.fr>

ARG USER_ID
ARG USER_GID
ENV USER_ID $USER_ID
ENV USER_GID $USER_GID

# Install prerequisites
RUN apt-get update && apt-get install -y wget
RUN apt-get update && apt-get install -y \
                       build-essential \
                       mesa-common-dev \
                       apt-utils \
                       cmake \
                       make \
                       locate \
                       git \
                       curl \
                       software-properties-common \
                       sudo \
                       protobuf-compiler

RUN apt-get update && apt-get install -y --fix-missing curl

RUN add-apt-repository -y ppa:ubuntu-toolchain-r/test
RUN apt-get update
RUN apt-get install -y gcc-5 g++-5
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-5 60 --slave /usr/bin/g++ g++ /usr/bin/g++-5



# Add basic user
ENV USERNAME icubuser
ENV PULSE_SERVER /run/pulse/native
RUN useradd -ms /bin/bash ${USERNAME} && \
    echo "$USERNAME:$USERNAME" | chpasswd && \
    usermod -aG sudo ${USERNAME} && \
    echo "$USERNAME ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/$USERNAME && \
    chmod 0440 /etc/sudoers.d/$USERNAME && \
    # Replace 1000 with your user/group id
    usermod  --uid ${USER_ID} ${USERNAME} && \
    groupmod --gid ${USER_GID} ${USERNAME}

RUN chown -R $USERNAME /usr/local/

# Change user
USER $USERNAME
WORKDIR /home/${USERNAME}
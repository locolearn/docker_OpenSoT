# icub-common image
FROM inria/gui

# Dockerfile maintainer
MAINTAINER Brice Clement, <brice.clement@inria.fr>

USER root

RUN apt-get update && apt-get install -y \
	subversion cmake-curses-gui \
	wget unzip openssh-server \
	sphinx-common python-dev python-numpy python-tk \
	libc6 libncurses5-dev \
	libace-dev \
	libgsl0-dev \
	libcv-dev libhighgui-dev libcvaux-dev libopencv-dev \
	libsdl1.2-dev \
	gfortran \
	libxmu-dev \
	libode4 libode-dev \
	freeglut3-dev \
	libgoocanvasmm-2.0-dev \
	swig

# QT5
RUN apt-get update && apt-get install -y \
	qt5-default qtbase5-dev qtdeclarative5-dev qtmultimedia5-dev libqt5svg5 libqwt-qt5-6 libqwt-qt5-dev

RUN apt-get install -y automake

USER $USERNAME

# install eigen-3.3.3
RUN wget "http://bitbucket.org/eigen/eigen/get/3.3.3.zip" -P /home/$USERNAME/
RUN unzip /home/$USERNAME/3.3.3.zip -d /home/$USERNAME/
RUN rm /home/$USERNAME/3.3.3.zip
RUN mv /home/$USERNAME/eigen-eigen-* /home/$USERNAME/eigen-3.3.3

USER root

RUN /bin/bash -c "cd /home/$USERNAME/eigen-3.3.3; \
	mkdir build; \
	cd build; \
	cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr ..; \
	make install;"

USER $USERNAME

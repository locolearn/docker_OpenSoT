#!/bin/sh

if [ -d "./OpenSoT/OpenSoT-superbuild" ]; then
  cd ./OpenSoT/OpenSoT-superbuild/

  echo "ModelInterfaceRBDL last stable commit on master branch : ac990f6c9b719603978a9ed2a412ef54bd032f7f"
  if [ -d "./external/ModelInterfaceRBDL"]; then
    cd ./external/ModelInterfaceRBDL
    git checkout ac990f6c9b719603978a9ed2a412ef54bd032f7f
    cd ../..
  fi

  echo "OpenSoT last stable commit on devel branch : fa1c666fbc85d3fcae9e12bda443692333102327"
  if [ -d "./external/OpenSoT"]; then
    cd ./external/OpenSoT
    git checkout devel
    git checkout fa1c666fbc85d3fcae9e12bda443692333102327
    cd ../..
  fi

  echo "XBotCoreModel last stable commit on master branch : 8d13c6b272c5073c98fbccb1ace5e853d66363c1"
  if [ -d "./external/XBotCoreModel" ]; then
      cd ./external/XBotCoreModel
      git checkout 8d13c6b272c5073c98fbccb1ace5e853d66363c1
      cd ../..
  fi

  echo "XBotInterface last stable commit on master branch : 56b4c865aac0fdaa59939c96ffa644ba4f923678"
  if [ -d "./external/XBotInterface" ]; then
    cd ./external/XBotInterface
    git checkout 56b4c865aac0fdaa59939c96ffa644ba4f923678
    cd ../..
  fi

  echo "YCM last stable commit : 538bec08ad3e32b4423fe008e79d0eee941e89e9"
  if [ -d "./external/YCM" ]; then
    cd ./external/YCM
    git checkout 538bec08ad3e32b4423fe008e79d0eee941e89e9
    cd ../..
  fi

  echo "sharedlibpp last stable commit on master branch : 6d4a102d47bea4bc48a2904f5b89e04d1b975d9f"
  if [ -d "./external/sharedlibpp"]; then
    cd ./external/sharedlibpp
    git checkout 6d4a102d47bea4bc48a2904f5b89e04d1b975d9f
    cd ../..
  fi

  echo "srdfdom_advr last stable commit on master branch : ff7de9e863bbe344fd8215775b41a178bcdad408"
  if [ -d "./external/srdfdom_advr"]; then
    cd ./external/srdfdom_advr
    git checkout ff7de9e863bbe344fd8215775b41a178bcdad408
    cd ../..
  fi

  echo "trajectory_utils last stable commit on origin/com branch : 17a3f1152d57115f1bba879c403930ab21c88cc9"
  if [ -d "./external/trajectory_utils"]; then
    cd ./external/trajectory_utils
    git checkout origin/com
    git checkout 17a3f1152d57115f1bba879c403930ab21c88cc9
    cd ../..
  fi
fi
